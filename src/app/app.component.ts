import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  name = 'dummy name';

  displayContent = false;

  items: string[] = ['Same', 'apple', 'food', 'meat'];

  imageUrl = 'http://www.mahala.co.za/wp-content/uploads/2011/07/janc.jpg';

  colorText = 'myBlueText';

  person = {
    'name': 'maphanga',
    'surname': 'nathi',
  };

  myStyleObject = {
    'color': 'red',
    'text-align': 'right'
  };

  perform() {
    console.log('perform');
    this.displayContent = !this.displayContent;
  }

}
